const webpack = require( 'webpack' ); // eslint-disable-line no-unused-vars
const path = require( 'path' );
const autoprefixer = require( 'autoprefixer' );
const fs = require( 'fs' ); // eslint-disable-line no-unused-vars
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );
const WebpackAssetsManifest = require( 'webpack-assets-manifest' );
const { CleanWebpackPlugin } = require( 'clean-webpack-plugin' );

const production = 'development' !== process.env.NODE_ENV;

module.exports = {
	entry: {
		bundle: './src/js/index',
		bundleAdmin: './src/js/admin/index',
		style: './src/sass/index',
		critical: './src/sass/index-critical',
		editor: './src/sass/index-editor',
	},
	output: {
		path: path.resolve( __dirname, 'dist' ),
		filename: 'js/[name].[chunkhash].min.js',
		publicPath: '../',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [ '@babel/preset-env' ],
					},
				},
			},
			{
				test: /\.scss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							ident: 'postcss',
							plugins: () => [
								autoprefixer( {
									grid: true,
								} ),
							],
						},
					},
					'sass-loader',
				],
			},
			{
				test: /.*\.(png|jpe?g|svg)$/i,
				use: {
					loader: 'url-loader',
					options: {
						limit: 5000,
						name: 'images/[name].[hash:7].[ext]',
					},
				},
			},
			{
				test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
				use: {
					loader: 'file-loader',
					options: {
						name: 'fonts/[name].[ext]',
					},
				},
			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin( {
			filename: 'css/[name].[chunkhash].min.css',
		} ),
		new WebpackAssetsManifest(),
		new CleanWebpackPlugin(),
	],
};

if ( production ) {
	module.exports.devtool = false;
}
