<?php
/**
 * The template for displaying the front page
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<?php get_header(); ?>

<div class="constrain">
	<?php
	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content/content', 'page' );
		endwhile;
	endif;
	?>
</div>

<?php
get_footer();
