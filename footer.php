<?php
/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

			</main>
		</div>

		<div class="constrain has-white-background-color ui-shadow">
			<div class="footer">
				<?php echo date( 'Y' ); // phpcs:ignore ?> Chtheme
			</div>
		</div>

		<?php wp_footer(); ?>
	</body>
</html>
