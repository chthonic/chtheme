<?php
/**
 * Manage functions specific to this theme.
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 * @version 1.1.0
 */

foreach ( glob( __DIR__ . '/inc/*.php' ) as $filename ) {
	require_once $filename;
}
