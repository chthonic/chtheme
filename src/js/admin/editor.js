/**
 * Manage block variations and filters.
 */
wp.domReady( () => {
	/* Retrieve registered blocks. */
	/*wp.blocks.getBlockTypes().forEach( ( block ) => {
		if ( _.isArray( block.styles ) ) {
			console.log( block.name, _.pluck( block.styles, 'name' ) );
		}
	});*/

	wp.blocks.unregisterBlockStyle( 'core/button', 'fill' );
	wp.blocks.unregisterBlockStyle( 'core/button', 'outline' );
	wp.blocks.unregisterBlockStyle( 'core/quote', 'large' );
	wp.blocks.unregisterBlockStyle( 'core/image', 'default' );
	wp.blocks.unregisterBlockStyle( 'core/image', 'rounded' );
} );
