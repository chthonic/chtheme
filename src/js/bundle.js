/**
 * Insert a toggle button for managing primary navigation visibility on smaller screens.
 *
 * @since 1.0.0
 */
( () => {
	const menu = document.getElementById( 'js-menu-drawer' );
	const toggle = document.createElement( 'button' );

	toggle.setAttribute( 'id', 'js-menu-toggle' );
	toggle.setAttribute( 'class', 'menu-toggle' );
	toggle.setAttribute( 'aria-expanded', false );
	toggle.setAttribute( 'aria-label', 'Navigation' );
	toggle.innerHTML = 'Menu';

	menu.insertAdjacentElement( 'beforebegin', toggle );
} )();

/**
 * Toggle navigation
 *
 * Manage primary navigation for smaller screens.
 *
 * @since 1.0.0
 */
( () => {
	const menu = document.getElementById( 'js-menu-drawer' );
	const toggleMenu = document.getElementById( 'js-menu-toggle' );

	toggleMenu.addEventListener( 'click', () => {
		toggleMenu.classList.toggle( 'is-active' );
		menu.classList.toggle( 'is-active' );

		const menuExpanded =
			'true' === toggleMenu.getAttribute( 'aria-expanded' ) || false;
		toggleMenu.setAttribute( 'aria-expanded', ! menuExpanded );
	} );
} )();
