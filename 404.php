<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<header>
	<h1>This page is missing.</h1>
</header>

<p>Sorry, nothing was found at this location.</p>

<?php
get_footer();
