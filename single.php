<?php
/**
 * The template for displaying a single post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<article>
	<div class="constrain">
		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content/content', 'page' );
		endwhile;
		?>
	</div>
</article>

<?php
get_footer();
