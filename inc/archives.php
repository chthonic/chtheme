<?php
/**
 * Manage post archive functions.
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 * @version 1.1.0
 */

/**
 * Hide the archive title prefix, "Category: ".
 *
 * @since 1.0.0
 * @param string $title Archive title.
 * @return string Archive title with inserted span around prefix.
 */
function chtheme_hide_archive_prefix( $title ) {
	$split_title = explode( ': ', $title, 2 );

	if ( ! empty( $split_title[1] ) ) {
		$title = $split_title[1];
		$title = '<span class="screen-reader-only">' . esc_html( $split_title[0] ) . ': </span>' . $title;
	}

	return $title;
}
add_filter( 'get_the_archive_title', 'chtheme_hide_archive_prefix' );

/**
 * Display navigation to next/previous set of posts when applicable.
 * Based on paging nav function from Twenty Fourteen
 *
 * @since 1.0.0
 */
function chtheme_archive_pagination() {
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}

	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );

	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}

	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.
	$links = paginate_links( array(
		'base'      => $pagenum_link,
		'format'    => $format,
		'total'     => $GLOBALS['wp_query']->max_num_pages,
		'current'   => $paged,
		'mid_size'  => 1,
		'add_args'  => array_map( 'urlencode', $query_args ),
		'prev_text' => __( 'Previous', 'yourtheme' ),
		'next_text' => __( 'Next', 'yourtheme' ),
		'type'      => 'list',
	) );

	if ( $links ) :
		?>
		<nav class="pagination" role="navigation">
			<?php echo wp_kses_post( $links ); ?>
		</nav>
		<?php
	endif;
}
