<?php
/**
 * Set up theme and sane defaults.
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 * @version 1.0.0
 */

/**
 * Set up theme defaults and register support for WordPress features.
 */
function chtheme_setup() {
	// Make the theme available for translation.
	load_theme_textdomain( 'chtheme' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );

	// Add block alignment support.
	add_theme_support( 'align-wide' );
}

add_action( 'after_setup_theme', 'chtheme_setup' );

/**
 * Define navigation menus available to the theme.
 */
register_nav_menus(
	array(
		'menu_primary' => __( 'Primary', 'chtheme' ), // template-parts/navigation/navigation-primary.php.
	)
);

/**
 * Disable default image compression.
 *
 * @param int $arg Compression quality.
 * @since 1.0.0
 */
function chtheme_no_wp_image_compression( $arg ) {
	return 100;
}

add_filter( 'jpeg_quality', 'chtheme_no_wp_image_compression' );

/**
 * Add a container element to embedded content.
 *
 * Helps target embedded videos with responsive styles.
 *
 * @since 1.0.0
 * @param mixed $html The parsed oembed content.
 */
function chtheme_add_wrapper_to_oembed( $html ) {
	return '<div class="embed-wrapper">' . $html . '</div>';
}

add_filter( 'embed_oembed_html', 'chtheme_add_wrapper_to_oembed', 10, 3 );

/**
 * Redirect default routes that are not required.
 *
 * Some websites do not require all of the default routes.
 * For example: /?s= is not required by a website that does
 * not offer search. Routes without a defined template will
 * default to the next available template, often index.php.
 */
function chtheme_hide_default_routes() {
	if ( ! is_admin() ) {
		/**
		 * Do not display the default category term archive.
		 */
		if ( is_category( 'uncategorised' ) ) {
			wp_safe_redirect( home_url() );
			exit;
		}

		/**
		 * Do not display author views.
		 */
		if ( is_author() ) {
			wp_safe_redirect( home_url() );
			exit;
		}

		/**
		 * Do not display search queries.
		 */
		if ( is_search() ) {
			wp_safe_redirect( home_url() );
			exit;
		}
	}
}

add_action( 'wp', 'chtheme_hide_default_routes' );

/**
 * Remove oEmbed provider functionality.
 *
 * @since 1.0.0
 */
function chtheme_disable_wp_embed_script() {
	wp_dequeue_script( 'wp-embed' );
}

add_action( 'wp_footer', 'chtheme_disable_wp_embed_script' );

/**
 * Detect if JavaScript available.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since 1.0
 */
function chtheme_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action( 'wp_head', 'chtheme_javascript_detection', 0 );

/**
 * Disable WordPress-native XML site map.
 *
 * @since 1.0.1
 */
function chtheme_disable_wp_xml_site_map() {
	add_filter( 'wp_sitemaps_enabled', '__return_false' );
}

add_action( 'after_setup_theme', 'chtheme_disable_wp_xml_site_map' );
