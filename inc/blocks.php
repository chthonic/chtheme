<?php
/**
 * Manage all block functions.
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 * @version 1.0.0
 */

/**
 * Whitelist of available blocks.
 *
 * @since 1.0.0
 * @version 1.0.1
 * @param Array $allowed_blocks The list of allowed blocks, empty by default.
 */
function chtheme_allowed_block_types( $allowed_blocks ) {
	return array(
		'core/block',
		'core/buttons',
		'core/button',
		'core/columns',
		'core/freeform',
		'core/group',
		'core/heading',
		'core/image',
		'core/list',
		'core/paragraph',
		'core/shortcode',
		'core/video',
		'core-embed/youtube',
		'chthonic/example',
		'chthonic/block',
		'chthonic/block-vanilla',
		'chthonic/card',
	);
}

add_filter( 'allowed_block_types', 'chtheme_allowed_block_types' );

/**
 * Manage custom colour palette.
 *
 * @since 1.0.0
 * @link https://www.billerickson.net/wordpress-color-palette-button-styling-gutenberg/
 */
function chtheme_manage_colour_palette() {
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'Black', 'chtheme' ),
				'slug'  => 'black',
				'color' => '#000',
			),
			array(
				'name'  => __( 'White', 'chtheme' ),
				'slug'  => 'white',
				'color' => '#fff',
			),
			array(
				'name'  => __( 'Brand', 'chtheme' ),
				'slug'  => 'brand',
				'color' => '#6a30f7',
			),
		)
	);

	add_theme_support( 'disable-custom-colors' );

}

add_action( 'after_setup_theme', 'chtheme_manage_colour_palette' );

/**
 * Manage block scripts.
 *
 * @since 1.0.1
 */
function chtheme_block_scripts() {
	/**
	 * Disable default block styles.
	 *
	 * @since 1.0.1
	 */
	wp_dequeue_style( 'wp-block-library' );
}

add_action( 'wp_enqueue_scripts', 'chtheme_block_scripts' );

/**
 * Manage block editor default functionality.
 *
 * @version 1.0.1
 */
function chtheme_manage_block_editor_defaults() {
	/**
	 * Use theme styles in editor.
	 */
	add_theme_support( 'editor-styles' );
	add_editor_style( get_theme_file_uri( '/' . chtheme_get_asset_path( 'editor.css' ) ) );

	/**
	 * Disable editor-editable font sizes.
	 *
	 * @since 1.0.0
	 */
	add_theme_support( 'editor-font-sizes' );

	/**
	 * Disable editor custom font sizes.
	 *
	 * @since 1.0.0
	 */
	add_theme_support( 'disable-custom-font-sizes' );

	/**
	 * Disable default block patterns.
	 *
	 * @since 1.0.1
	 */
	remove_theme_support( 'core-block-patterns' );

	/**
	 * Disable gradients.
	 *
	 * @since 1.0.0
	 */
	add_theme_support( 'disable-custom-gradients' );
	add_theme_support( 'editor-gradient-presets', array() );
}

add_action( 'after_setup_theme', 'chtheme_manage_block_editor_defaults' );

/**
 * Disable the block directory.
 *
 * @since 1.0.1
 */
function chtheme_disable_block_directory() {
	remove_action( 'enqueue_block_editor_assets', 'wp_enqueue_editor_block_directory_assets' );
	remove_action( 'enqueue_block_editor_assets', 'gutenberg_enqueue_block_editor_assets_block_directory' );
}

add_action( 'plugins_loaded', 'chtheme_disable_block_directory' );
