<?php
/**
 * Manage block assets for admin editor.
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 * @version 1.0.0
 */

/**
 * Manages block editor defaults.
 *
 * @since 1.0.0
 */
function chtheme_block_editor_assets() {
	$js_dependencies = array( 'wp-blocks', 'wp-dom' );

	wp_enqueue_script(
		'chtheme-block-editor',
		get_theme_file_uri( '/' . chtheme_get_asset_path( 'bundleAdmin.js' ) ),
		$js_dependencies,
		wp_get_theme()->get( 'Version' ),
		true
	);
}

add_action( 'enqueue_block_editor_assets', 'chtheme_block_editor_assets', 1, 1 );
