<?php
/**
 * Manages all block patterns
 *
 * @link https://developer.wordpress.org/reference/functions/register_block_pattern/
 * @link https://developer.wordpress.org/reference/functions/register_block_pattern_category/
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 */

/**
 * Registers the block pattern category
 *
 * @since 1.0.0
 */
if ( function_exists( 'register_block_pattern_category' ) ) {
	register_block_pattern_category(
		'chtheme',
		array( 'label' => __( 'Chtheme', 'chtheme' ) )
	);
}

/**
 * Registers block patterns
 *
 * @since 1.0.0
 */

if ( function_exists( 'register_block_pattern' ) ) :
	register_block_pattern(
		'chtheme/images-two-column',
		array(
			'categories'  => array( 'chtheme' ),
			'content'     => "<!-- wp:buttons {\"align\":\"center\"} -->\n<div class=\"wp-block-buttons aligncenter\"><!-- wp:button {\"backgroundColor\":\"very-dark-gray\",\"borderRadius\":0} -->\n<div class=\"wp-block-button\"><a class=\"wp-block-button__link has-background has-very-dark-gray-background-color no-border-radius\">" . esc_html__( 'Button One', 'my-plugin' ) . "</a></div>\n<!-- /wp:button -->\n\n<!-- wp:button {\"textColor\":\"very-dark-gray\",\"borderRadius\":0,\"className\":\"is-style-outline\"} -->\n<div class=\"wp-block-button is-style-outline\"><a class=\"wp-block-button__link has-text-color has-very-dark-gray-color no-border-radius\">" . esc_html__( 'Button Two', 'my-plugin' ) . "</a></div>\n<!-- /wp:button --></div>\n<!-- /wp:buttons -->",
			'description' => 'Test for setting up block patterns',
			'keywords'    => '',
			'title'       => 'Images: 2-column',
		)
	);
endif;
