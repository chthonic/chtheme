<?php
/**
 * Manage theme file loading.
 *
 * @package Chthonic/Chtheme
 * @since 1.0.0
 * @version 1.0.0
 */

/**
 * Use assets with hashed names.
 *
 * Matches a filename against a hash manifest and returns the hash file name if
 * it exists.
 *
 * @param  string $filename Original name of the file.
 * @return string $filename Hashed name version of a file.
 */
function chtheme_get_asset_path( $filename ) {
	$manifest_path = get_template_directory_uri() . '/dist/manifest.json';

	if ( ! empty( $manifest_path ) ) {
		$request  = wp_remote_get( $manifest_path );
		$manifest = json_decode( wp_remote_retrieve_body( $request ) );

		if ( array_key_exists( $filename, $manifest ) ) {
			return 'dist/' . $manifest->$filename;
		}
	}

	return $filename;
}

/**
 * Inline critical CSS in <head>.
 *
 * @since 1.0.0
 */
function chtheme_critical_css() {
	$file = wp_remote_get( get_theme_file_uri( '/' . chtheme_get_asset_path( 'critical.css' ) ) );

	if ( is_array( $file ) && ! is_wp_error( $file ) ) :
		$critical_css = $file['body'];
		return wp_add_inline_style( 'chtheme', $critical_css );
	endif;
}

/**
 * Target stylesheets to load asynchronously.
 *
 * Alternative to legacy preload pattern.
 *
 * @since 1.0.0.
 * @param string $tag The HTML tag.
 * @param string $handle  The style ID.
 * @param string $src     The stylesheet URL.
 * @link https://timkadlec.com/remembers/2020-02-13-when-css-blocks/
 */
function chtheme_load_async_css( $tag, $handle, $src ) {
	if ( ! isset( $_COOKIE['stylesLoaded'] ) ) :
		if ( 'chtheme' === $handle ) :
			$tag = '<link rel="stylesheet" id="' . $handle . '" href="' . esc_url( $src ) . '" type="text/css" media="print" onload="this.media=\'all\'" />' . PHP_EOL;
		endif;
	endif;

	return $tag;
}

add_filter( 'style_loader_tag', 'chtheme_load_async_css', 10, 3 );

/**
 * WordPress script enqueue.
 *
 * Registering files via the wp_enqueue_* function avoids
 * script conflicts when the same files are loaded by a
 * plugin or other 3rd-party.
 *
 * @since 1.0
 */
function chtheme_scripts() {
	// Load the theme stylesheet if it is cached.
	if ( isset( $_COOKIE['stylesLoaded'] ) ) :
		wp_enqueue_style( 'chtheme', get_theme_file_uri( '/' . chtheme_get_asset_path( 'style.css' ) ), null, null, 'all' );

	else :
		/**
		 * Load critical CSS inline, load stylesheets asynchronously.
		 * Depends on chtheme_load_async_css() to switch stylesheet media type from 'print' to 'all'.
		 */
		?>
		<script>
			function cookie(e,i,o){if(void 0===i){var t="; "+window.document.cookie,n=t.split("; "+e+"=");return 2===n.length?n.pop().split(";").shift():null}i===!1&&(o=-1);var r;if(o){var c=new Date;c.setTime(c.getTime()+24*o*60*60*1e3),r="; expires="+c.toGMTString()}else r="";window.document.cookie=e+"="+i+r+"; path=/"}

			if( ! cookie( 'stylesLoaded' ) ) {
			cookie( 'stylesLoaded', true, 7 );
			}
		</script>

		<noscript><link id="chtheme-noscript" rel="stylesheet" href="<?php echo get_theme_file_uri( '/' . chtheme_get_asset_path( 'style.css' ) ); ?>" type="text/css" media="all"></noscript>

		<?php
		wp_enqueue_style( 'chtheme', get_theme_file_uri( '/' . chtheme_get_asset_path( 'style.css' ) ), null, null, 'print' );
		chtheme_critical_css();
	endif;

	// Load theme javascript.
	wp_register_script( 'chtheme-js', get_theme_file_uri( '/' . chtheme_get_asset_path( 'bundle.js' ) ), null, null, true );

	/**
	 * Pass PHP variables to a global JS object.
	 *
	 * Usage examples:
	 *
	 * - Pass an option value: 'siteURL' => get_option( 'siteurl' )
	 * - Pass a a conditional value: 'isFront' => is_front_page()
	 * - Pass output of a custom function: 'value' => chtheme_get_value()
	 *
	 * Keys should be set using camelCase.
	 *
	 * @link https://developer.wordpress.org/reference/functions/wp_localize_script/
	 */
	$chtheme_js_localised = array(
		'siteUrl'     => get_option( 'siteurl' ), // Returns the site URL.
		'isFront'     => is_front_page(),         // Returns true or false.
		'currentPage' => get_the_title(),         // Returns the current page title.
		'isLoggedIn'  => is_user_logged_in(),     // Returns true or false.
	);

	wp_localize_script( 'chtheme-js', 'chtheme', $chtheme_js_localised );
	wp_enqueue_script( 'chtheme-js' );

	// Push jQuery to the footer, if exists.
	wp_scripts()->add_data( 'jquery', 'group', 1 );
	wp_scripts()->add_data( 'jquery-core', 'group', 1 );
	wp_scripts()->add_data( 'jquery-migrate', 'group', 1 );
}

add_action( 'wp_enqueue_scripts', 'chtheme_scripts' );
