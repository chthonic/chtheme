<?php
/**
 * The template for displaying the header
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>
	</head>

	<body <?php body_class(); ?>>
		<a href="#skip-to" class="skip-to-content">Skip to content</a>

		<div class="constrain has-white-background-color ui-shadow">
			<?php get_template_part( 'template-parts/layout/layout', 'header' ); ?>
		</div>

		<div class="rhythm">
			<main id="skip-to" tabindex="-1">
