<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<header>
	<?php if ( have_posts() ) : ?>
		<?php
		the_archive_title( '<h1>', '</h1>' );
		the_archive_description();
		?>
	<?php endif; ?>
</header>

<?php if ( have_posts() ) : ?>
	<section>
		<?php while ( have_posts() ) : ?>

			<article>
				<?php
				the_post();
				/**
				 * Include the specific template for the content.
				 *
				 * If variables need to be passed to the template part,
				 * use `include locate_template('path/to/file.php')` instead:
				 *
				 * - `include locate_template()` will pass variables to the template part
				 * - `get_template_part` does not.
				 */
				get_template_part( 'template-parts/content/content', 'excerpt' );
				?>
			</article>

		<?php endwhile; ?>
	</section>

	<?php get_template_part( 'template-parts/navigation/navigation', 'pagination' ); ?>

	<?php
else :
	get_template_part( 'template-parts/content/content', 'none' );
endif;
?>

<?php
get_footer();
