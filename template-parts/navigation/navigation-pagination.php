<?php
/**
 * Displays the post pagination
 *
 * Pagination logic located in inc/archive-functions.php.
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

chtheme_archive_pagination();
