<?php
/**
 * Displays the primary navigation
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<?php
$args = array(
	'theme_location' => 'menu_primary',       // The navigation menu's handle.
	'fallback_cb'    => 'Primary',            // The label for the menu.
	'menu_id'        => 'js-menu--primary',
	'menu_class'     => 'menu menu--primary', // Add this class to the menu parent tag.
	'container'      => false,                // Do not add a wrapping tag.
	'item_spacing'   => 'discard',            // Strip white-space between children.
);

wp_nav_menu( $args );
