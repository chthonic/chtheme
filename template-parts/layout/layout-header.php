<?php
/**
 * Template part for header
 *
 * Includes logo and primary navigation.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<div class="header">
	<div id="js-logo">
		<?php if ( ! is_front_page() ) : ?>
			<a class="go-home" href="<?php echo esc_url( site_url() ); ?>"><?php get_template_part( 'template-parts/components/component', 'logo' ); ?></a>

		<?php else : ?>
			<?php get_template_part( 'template-parts/components/component', 'logo' ); ?>
		<?php endif; ?>
	</div>

	<div id="js-menu-drawer" class="menu-drawer">
		<nav><?php get_template_part( 'template-parts/navigation/navigation', 'primary' ); ?></nav>
	</div>
</div>
