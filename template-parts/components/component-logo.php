<?php
/**
 * Displays an SVG wordmark
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<div class="wordmark">
	[logo]
</div>
