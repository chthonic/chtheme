<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<header>
	<?php the_title( '<h1>', '</h1>' ); ?>
</header>

<?php the_content(); ?>
