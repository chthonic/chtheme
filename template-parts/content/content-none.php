<?php
/**
 * Template part for displaying a message that a page cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<header>
	<h1>There&rsquo;s nothing here right now</h1>
</header>

<p>Please check back soon.</p>
