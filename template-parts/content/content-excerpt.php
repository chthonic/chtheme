<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Post Archives and Search results.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Chthonic/Chtheme
 * @since 1.0
 * @version 1.0
 */

?>

<header>
	<?php the_title( sprintf( '<h2><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
</header>

<div>
	<?php the_excerpt(); ?>
</div>
