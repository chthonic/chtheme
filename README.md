# A starter WordPress theme for Chthonic projects

This is a starter theme for WordPress theme development. It is fairly "bare bones".

WordPress 5.0 introduced a "block" editor. A downside for front-end performance is a bundled stylesheet for seldom-used blocks, including legacy styles for blocks with updated markup or naming conventions. This theme manages this by:

- whitelisting a small set of core native blocks: buttons, columns, freeform, group, heading, image, list, paragraph, shortcode, youtube.
- including structural CSS for whitelisted blocks.

Also included:

- unrequired native block styles and UI tools are disabled (ongoing as various editor APIs improve).
- custom block colour palette is enabled.
- minor presentation CSS for initial sketches, including [an approach](https://utopia.fyi/) for implementing CSS "locks" to help manage element sizing.
- a [modern CSS reset](https://hankchizljaw.com/wrote/a-modern-css-reset/) to set sensible defaults.
- a responsive visual "jig" grid for development (enabled by default).
- a focus on accessibility.

Based on an earlier [Chthonic starter theme](https://bitbucket.org/chthonic/chthonic-theme-wp/).

## Linting
----------

All rulesets are from [WordPress Coding Standards](https://github.com/WordPress-Coding-Standards):

- PHP with [PHP CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
- JS with [ESLint](https://eslint.org)
- CSS with [stylelint](https://github.com/stylelint/stylelint)

## Getting started
------------------

- Clone this repository to the `themes` folder of a local development WordPress install.
- Rename the theme folder from `chthonic-theme-wp` to `your-project-name`.

## Search and replace
---------------------

Variations on "chtheme" have been used as a placeholder for the project's name. To update this starter theme with new project details, run search-and-replace for the following:

- Replace `https://chtheme.test` with `https://your-project-url`
- Replace `Chtheme` with `Your Project Name`
- Replace `chtheme` with `your-project-name`
- Replace `chthm` with a unique function prefix
- Replace the content of README.md with a description of the project

**Important**: Remember to search-and-replace with "match case" enabled.

## Build the theme
------------------

The following presumes `npm` and `composer` are installed.

### Install required packages

- `npm install`
- `composer install`

### Set up WPCS

Set PHPCS path:
`./vendor/bin/phpcs --config-set installed_paths C:/Users/Daniel/Dropbox/projects/wpcs`

VS Code:

- Composer JSON Path `composer.json`
- Executable Path `vendor/squizlabs/php_codesniffer/bin/phpcs.bat`

### Run webpack

webpack is set up for development and production environments (there are no tests).

- Development: `npm start`
- Production: `npm run build`

### Success?

Look for a `dist` folder in the theme root. If it isn't present, something has gone wrong.

## Todo
----------

- Tests
- Some functions should be moved to a plugin
- Automate renaming placeholder variations on 'chtheme'.